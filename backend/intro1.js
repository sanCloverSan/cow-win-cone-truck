import Web3 from 'web3';
import dotenv from 'dotenv';

async function main() {
    const network = process.env.ETHEREUM_NETWORK;
    // console.log(network);
    const web3 = new Web3(`https://${network}.infura.io/v3/${process.env.INFURA_PROJECT_ID}`);

    // const signer = web3.eth.accounts.privateKeyToAccount(process.env.SIGNER_PRIVATE_KEY);
    // web3.eth.account.wallet.add(signer);

    //Some random address from etherscan transaction history
    const someAddress = "0x52bc44d5378309ee2abf1539bf71de1b7d7be3b5";

    let balance = "";
    await web3.eth.getBalance(someAddress, (err, bal) => {
        if (err) console.log(err);
        balance = bal;
    }).then((data) => Promise.resolve(data));

    console.log(web3.utils.fromWei(balance, "ether"));

}

dotenv.config();
main()